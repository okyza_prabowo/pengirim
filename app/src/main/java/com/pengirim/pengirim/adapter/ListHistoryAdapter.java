package com.pengirim.pengirim.adapter;

/**
 * Created by ASUS on 27/11/2015.
 */

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pengirim.pengirim.R;

import java.util.ArrayList;
import java.util.List;

public class ListHistoryAdapter extends ArrayAdapter<String> {
    private final Activity context;
    List<String> RESI = new ArrayList<String>();
    Dialog dialogAdd, dialogDelete;
    String namaCategory, idCategory;

    public ListHistoryAdapter(Activity context,
                              List<String> resi) {
        super(context, R.layout.list_single, resi);

        this.context = context;
        this.RESI = resi;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.tvTitle);
        txtTitle.setText(RESI.get(position));
        return rowView;
    }


}