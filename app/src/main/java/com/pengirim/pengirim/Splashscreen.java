package com.pengirim.pengirim;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.os.Handler;
import android.widget.Toast;

import com.pengirim.pengirim.services.Token;
import com.pengirim.pengirim.utils.Const;


public class Splashscreen extends Activity {

    private static int SPLASH_TIME_OUT = 2000;
    SharedPreferences pref;
    MyReceiver myReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        setContentView(R.layout.activity_splashscreen);

        // jalankan service
        pref = getApplicationContext().getSharedPreferences(Const.MyPREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        Boolean stattoken = pref.getBoolean("statustoken", false);
        //if(stattoken) {
        //      System.out.println("Token masih hidup");
        //}
        //else {
        //    System.out.println("Token sudah mati");
            startService(new Intent(getBaseContext(), Token.class));

        //}

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(Splashscreen.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub

        //Register BroadcastReceiver
        //to receive event from our service
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.MY_ACTION);
        registerReceiver(myReceiver, intentFilter);

        super.onStart();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        unregisterReceiver(myReceiver);
        super.onStop();
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

            int datapassed = arg1.getIntExtra("DATAPASSED", 0);

            Toast.makeText(Splashscreen.this, "Komunikasi dengan server gagal",
                    Toast.LENGTH_LONG).show();
        }
    }
}
