package com.pengirim.pengirim;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.pengirim.pengirim.utils.Const;

public class HasilLacak extends Activity {

    private WebView webView;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        setContentView(R.layout.activity_lacak);
        //set webview sebagai tampilan utama
        if (!DetectConnection.checkInternetConnection(this)) {
            Toast.makeText(getApplicationContext(), "Tidak ada koneksi ke server", Toast.LENGTH_SHORT).show();
            finish();
        } else{
            webView = (WebView) findViewById(R.id.web_view);
            webView.setWebViewClient(new MyWebViewClient());
            webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 4.2; en-us; Xoom Build/HRI39) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13");
            openBrowser();
        }
    }

    @SuppressLint("SetJavaScriptEnabled") private void openBrowser()
    {
        //memanggil

        pref = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
        String uri = pref.getString("uri", "");

        //String url = "file:///storage/sdcard1/" + uri + ".html";
        String url = "file:///storage/emulated/0/" + uri + ".html";

        //webView.clearCache(true);

        //seting mobile agent

        //instance webchrome client baru
        WebChromeClient wcc = new WebChromeClient();
        webView.setWebChromeClient(wcc);
        webView.getSettings().setJavaScriptEnabled(true);
        //mengaktifkan built in zoom controls
        //webView.getSettings().setBuiltInZoomControls(true);
        //meload URL
        webView.loadUrl(url);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public static class DetectConnection {

        public static boolean checkInternetConnection(Context context) {

            ConnectivityManager con_manager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            if (con_manager.getActiveNetworkInfo() != null
                    && con_manager.getActiveNetworkInfo().isAvailable()
                    && con_manager.getActiveNetworkInfo().isConnected()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
