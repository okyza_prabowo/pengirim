package com.pengirim.pengirim.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pengirim.pengirim.model.History;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Okyza Maherdy Prabowo on 01/03/2016.
 */
public class HistoryHandler extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "historyManager";

    // History table name
    private static final String TABLE_HISTORY = "history";

    // History Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_RESI = "resi";

    public HistoryHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_HISTORY + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + KEY_RESI + " TEXT"
                + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);

        // Create tables again
        onCreate(db);
    }

    public void addHistory(String history) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_RESI, history); // Ambil resi

        // Inserting Row
        db.insert(TABLE_HISTORY, null, values);
        db.close(); // Closing database connection
    }

    // Getting All History
    public List<String> getAllHistory() {
        List<String> historyList = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_HISTORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //History history = new History();
                //history.setID(Integer.parseInt(cursor.getString(0)));
                //history.setResi(cursor.getString(1));
                // Adding history to list
                //historyList.add(history);
                historyList.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        // return history list
        return historyList;
    }

    //delete all history
    public void deleteHistory()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HISTORY, null, null);
        db.close();
    }

    public int getHistoryRow() {
        int row = 0;
        String selectQuery = "SELECT  * FROM " + TABLE_HISTORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        row = cursor.getCount();

        return row;
    }
}
