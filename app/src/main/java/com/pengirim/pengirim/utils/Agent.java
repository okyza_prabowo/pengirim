package com.pengirim.pengirim.utils;

/**
 * Created by Okyza Maherdy Prabowo on 01/06/2016.
 */
public class Agent {

    String name;
    int weight;
    int number;
    int weighted;

    // constructor
    public Agent(String name, int weight, int number){
        this.name = name;
        this.number = number;
        this.weight = weight;
    }

    public int add(int value)
    {
        this.weighted = this.weighted + value;
        return this.weighted;
    }

    public void reset()
    {
        this.weighted = this.weight;
    }
}
