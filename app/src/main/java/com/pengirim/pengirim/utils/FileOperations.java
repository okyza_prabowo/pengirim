package com.pengirim.pengirim.utils;

/**
 * Created by Okyza Maherdy Prabowo on 17/11/2015.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.util.Log;

public class FileOperations {
    public FileOperations() {

    }

    public Boolean write(String fname, String fcontent){
        try {

            String fpath = "/storage/sdcard1/datalatih/"+fname+".arff";

            File file = new File(fpath);

            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fcontent);
            bw.close();

            Log.d("Sukses atuh","Sukses atuh");
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public String read(String fname){

        BufferedReader br = null;
        String response = null;

        try {

            StringBuffer output = new StringBuffer();
            String fpath = "/storage/sdcard1/"+fname+".txt";

            br = new BufferedReader(new FileReader(fpath));
            String line = "";
            while ((line = br.readLine()) != null) {
                output.append(line +"n");
            }
            response = output.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }
        return response;
    }

    public double[] readData(String fname){
        double[] dataSensor = new double[4];
        double[] temp = new double[3];
        double X = 0;
        double Y = 0;
        double Z = 0;
        double counter = 0;

        BufferedReader br = null;
        String delimiter = ",";

        try {
            StringBuffer output = new StringBuffer();
            String fpath = "/storage/sdcard1/"+fname+".txt";

            br = new BufferedReader(new FileReader(fpath));
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] tokens = line.split(delimiter);
                temp[0] = Double.parseDouble(tokens[0]); //X value
                temp[1] = Double.parseDouble(tokens[1]); //Y value
                temp[2] = Double.parseDouble(tokens[2]); //Z value
                X = X + temp[0]; //acc X
                Y = Y + temp[1]; //acc Y
                Z = Z + temp[2]; //acc Z
                counter++; //acc total data
                //System.out.println(tokens[0]);
                //System.out.println(tokens[1]);
                //System.out.println(tokens[2]);

            }
            dataSensor[0] = X;
            dataSensor[1] = Y;
            dataSensor[2] = Z;
            dataSensor[3] = counter;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return dataSensor;
    }
}
