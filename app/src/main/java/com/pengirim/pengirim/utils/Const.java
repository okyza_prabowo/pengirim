package com.pengirim.pengirim.utils;

/**
 * Created by Okyza Maherdy Prabowo on 09/03/2016.
 */
public class Const {

    //url
    public static final String url_token= "http://fierce-coast-9197.herokuapp.com/api/v1/tokens/exchange_key_for_token" ;
    public static final String url_lacak= "http://fierce-coast-9197.herokuapp.com/api/v1/search/";
    public static final String url_feedback= "http://fierce-coast-9197.herokuapp.com/api/v1/feedbacks";
    public static final String url_daftar= "https://www.pengirim.com/users/sign-up";
    public static final String url_masuk = "https://www.pengirim.com/users/login";

    //encrypt key
    public static final String encrypt_key = "pEngIrImdotComOK" ;

    //local storage
    public static final String MyPREFERENCES = "TokenPrefs" ;

    //service label
    public final static String MY_ACTION = "MY_ACTION";
}
