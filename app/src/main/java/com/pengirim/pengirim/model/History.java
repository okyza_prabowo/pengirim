package com.pengirim.pengirim.model;

/**
 * Created by Okyza Maherdy Prabowo on 01/03/2016.
 */
public class History {

    int id;
    String resi;

    public History(){}

    // constructor
    public History(int id, String resi){
        this.id = id;
        this.resi = resi;
    }

    // constructor
    public History(String resi){
        this.resi = resi;
    }
    // getting ID
    public int getID(){
        return this.id;
    }

    // setting id
    public void setID(int id){
        this.id = id;
    }

    // getting resi
    public String getResi(){
        return this.resi;
    }

    // setting name
    public void setResi(String resi){
        this.resi = resi;
    }

}
