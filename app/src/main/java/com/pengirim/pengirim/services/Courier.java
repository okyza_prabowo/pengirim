package com.pengirim.pengirim.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Okyza Maherdy Prabowo on 01/03/2016.
 */
public class Courier extends Service {

    SharedPreferences pref;
    public static final String MyPREFERENCES = "TokenPrefs";

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        System.out.println("Getting Courier");
        String android_id;

        //ambil device id
        android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        pref = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String token = pref.getString("token", "");

        try
        {
            new MyAsyncTask().execute(token, android_id);
            System.out.println("Request Berjalan");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        stopSelf();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Destroy Courier");
    }

    public String requestCourier(String id, String mobile_token){

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://fierce-coast-9197.herokuapp.com/api/v1/search/courier_code");
        String text = "";

        try {
            // Add your data
            List nameValuePairs = new ArrayList(1);
            System.out.println("Token "+id);
            System.out.println("Device ID "+ mobile_token);
            nameValuePairs.add(new BasicNameValuePair("token", id));
            nameValuePairs.add(new BasicNameValuePair("mobile_token", "mas_oky" ));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;

            while((current = bis.read()) != -1){
                baf.append((byte)current);
            }

            /* Convert the Bytes read to a String. */
            text = new String(baf.toByteArray());


        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return text;
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, Double> {
        @Override
        protected Double doInBackground(String... params) {
            // TODO Auto-generated method stub
            String response = requestCourier(params[0], params[1]);
            System.out.println("Response: "+ response);

            //simpan di local storage
            /*
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("token", getToken(response));
            editor.putBoolean("statustoken", true);
            editor.commit();
            */

            stopSelf();
            return null;
        }
    }
}
