package com.pengirim.pengirim.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

import com.pengirim.pengirim.utils.Const;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Okyza Maherdy Prabowo on 28/02/2016.
 */
public class Token extends Service {

    public static final boolean DEBUG = true;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        System.out.println("Getting Token");
        createToken();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Destroy Service Token");
    }

    public void createToken()
    {
        String android_id;

        //ambil device id
        android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        try
        {
            String dataRequest = encrypt_data(android_id);
            new MyAsyncTask().execute(dataRequest);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String postData(String id){

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Const.url_token);
        String text = "";
        String default_token = "pigh8Uu0aF55ZMWmva+Z6g==\n";

        try {
            // add params
            List nameValuePairs = new ArrayList(1);
            nameValuePairs.add(new BasicNameValuePair("mobile_token", "mas_oky"));
            nameValuePairs.add(new BasicNameValuePair("mobile_token_key", default_token));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;

            while((current = bis.read()) != -1){
                baf.append((byte)current);
            }

            /* Convert the Bytes read to a String. */
            text = new String(baf.toByteArray());


        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return text;
    }

    public String encrypt_data(String data) throws Exception {
        String key = Const.encrypt_key;
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encValue = cipher.doFinal(data.getBytes());
        String hasil = Base64.encodeToString(encValue, Base64.DEFAULT);
        if (DEBUG) {
            System.out.println("Asli :" + data);
            System.out.println("Enkripsi : " + hasil);
        }
        return hasil;
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, Double> {
        @Override
        protected Double doInBackground(String... params) {
            // TODO Auto-generated method stub
            String response = postData(params[0]);
            System.out.println("Response: "+ response);
            System.out.println("Token ID: "+ getToken(response));

            //simpan di local storage
            SharedPreferences sharedpreferences = getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("token", getToken(response));
            editor.putBoolean("statustoken", true);
            editor.commit();

            stopSelf();
            return null;
        }
    }

    public String getToken(String response)
    {
        String token = "";

        //parsing json
        try {
            JSONObject reader = new JSONObject(response);
            JSONObject data = reader.getJSONObject("data");
            token = data.getString("token");
        }
        catch(JSONException jex)
        {
            jex.printStackTrace();
            Intent intent = new Intent();
            intent.setAction(Const.MY_ACTION);

            intent.putExtra("DATAPASSED", 0);

            sendBroadcast(intent);
        }
        return token;
    }
}
