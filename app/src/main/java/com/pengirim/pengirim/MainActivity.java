package com.pengirim.pengirim;

import com.pengirim.pengirim.adapter.NavDrawerListAdapter;
import com.pengirim.pengirim.fragment.AboutFragment;
import com.pengirim.pengirim.fragment.DaftarFragment;
import com.pengirim.pengirim.fragment.FeedbackFragment;
import com.pengirim.pengirim.fragment.HistoryFragment;
import com.pengirim.pengirim.fragment.LacakFragment;
import com.pengirim.pengirim.fragment.MasukFragment;
import com.pengirim.pengirim.model.NavDrawerItem;
import com.pengirim.pengirim.services.Courier;
import com.pengirim.pengirim.services.Token;
import com.pengirim.pengirim.utils.Const;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.provider.Settings.Secure;

public class MainActivity extends Activity {
    RelativeLayout leftRL;
    DrawerLayout drawerLayout;
    private ListView mDrawerList;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        //ambil android_id
        android_id = Secure.getString(this.getContentResolver(),Secure.ANDROID_ID);

        //inisialisasi menu drawer kiri
        leftRL = (RelativeLayout)findViewById(R.id.whatYouWantInLeftDrawer);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        pengirimMenu();
        if (savedInstanceState == null) {
            // item pada saat pertama kali ditampilkan
            displayView(0);
        }
    }

    @Override
    public void onBackPressed() {
        stopService(new Intent(getBaseContext(), Token.class));
        finish();
    }

    /**
     * Menampilkan menu drawer
     * */
    private void displayView(int position) {
        // update drwarer
        Fragment fragment = null;

        switch (position) {
            case 0:
                hideKeyboard();
                fragment = new LacakFragment();
                break;
            case 1:
                hideKeyboard();
                fragment = new HistoryFragment();
                break;
            case 2:
                hideKeyboard();
                fragment = new FeedbackFragment();
                break;
            case 3:
                hideKeyboard();
                fragment = new AboutFragment();
                break;
            case 4:
                //fragment = new MasukFragment();
                hideKeyboard();
                fragment = new DaftarFragment();
                break;
            case 5:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Const.url_masuk));
                startActivity(browserIntent);
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment).commit();

            // update selected item and title, then close the drawer
            //mDrawerList.setItemChecked(position, true);
            //mDrawerList.setSelection(position);
            //setTitle(navMenuTitles[position]);
            //drawerLayout.closeDrawer(mDrawerList);

            drawerLayout.closeDrawer(leftRL);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    public void pengirimMenu()
    {
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerList = (ListView) findViewById(R.id.lvMenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));

        // Recycle the typed array
        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedFromList = String.valueOf(position);
                //Toast.makeText(getApplicationContext(),selectedFromList,Toast.LENGTH_SHORT).show();
                displayView(position);
            }
        });

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
    }

    public void onLeft(View view)
    {
        drawerLayout.openDrawer(leftRL);
    }

    public void onRight(View view)
    {
        displayView(0);
    }

    public void hideKeyboard()
    {
        try
        {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        catch (Exception e)
        {
            // Ignore exceptions if any
            Log.e("KeyBoardUtil", e.toString(), e);
        }
    }
}