package com.pengirim.pengirim.fragment;

/**
 * Created by Aira on 31/01/2016.
 */
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pengirim.pengirim.R;

public class AboutFragment extends Fragment {

    public AboutFragment(){}

    String p1 = "<p> <b>Pengirim</b> merupakan sebuah layanan pengelolaan resi pengiriman " +
            "barang. Kami mampu membuat pelacakan kiriman barang menjadi lebih mudah </p>";
    String p2 = "<p> <b>Pengirim</b> memudahkan pengguna jasa pengiriman barang untuk mengorganisasi" +
            "kumpulan nomor resi, memantau status paket, dan membagikannya kepada orang lain " +
            "<b>Pengirim</b> juga menjadi penghubung antara penerima paket, agen pengiriman,pengirim paket</p>";
    String p3 = "<p> Aplikasi <i>mobile</i> ini akan terus dikembangkan. Untuk menikmati fitur lengkap <b>Pengirim</b>" +
            ", silakan akses melalui <a href=\"www.pengirim.com\">www.pengirim.com</a></p>";
    String dev = "<p>Developer:<br>Adhiguna Utama S.</p>";
    String des = "<p>UX & UI Designer:<br>Fariz Sofyan M.<br>Adnan Hidayat P.</p>";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        TextView view = (TextView) rootView.findViewById(R.id.txtpar1);
        Spanned result = Html.fromHtml(p1 + p2 + p3 + dev + des);
        view.setText(result);

        return rootView;
    }


}
