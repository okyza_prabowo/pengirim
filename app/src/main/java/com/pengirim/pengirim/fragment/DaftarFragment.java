package com.pengirim.pengirim.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pengirim.pengirim.R;
import android.app.Fragment;
import android.widget.Button;
import android.widget.TextView;

import com.pengirim.pengirim.utils.Const;

public class DaftarFragment extends Fragment {

    public DaftarFragment(){}

    String p1 = "<p> Ayo bergabung dengan <b>Pengirim</b>. Solusi yang membuat aktivitas jual-beli <i>online</> makin mudah dan efisien." +
            "Nikmati berbagai manfaat dan kemudahan dengan menjadi pengguna <b>Pengirim</b>:</p>";
    String p2 = "- Simpan dan pantau resi pengiriman dalam satu tempat <br>";
    String p3 = "- Resi aktif yang diperbarui secara periodik<br>";
    String p4 = "- Pantau data analisis riwayat pengiriman<br>";
    String p5 = "- Notifikasi email <br>";
    String p6 = "- dan berbagai manfaat lainnya<br>";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_masuk, container, false);

        TextView view = (TextView) rootView.findViewById(R.id.txtpar1);
        Spanned result = Html.fromHtml(p1 + p2 + p3 + p4 + p5 + p6);
        view.setText(result);

        Button button = (Button) rootView.findViewById(R.id.bdaftar);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Const.url_daftar));
                startActivity(browserIntent);

            }

        });

        return rootView;
    }
}
