package com.pengirim.pengirim.fragment;

/**
 * Created by Okyza Maherdy Prabowo on 31/01/2016.
 */
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.evgenii.jsevaluator.JsEvaluator;
import com.evgenii.jsevaluator.interfaces.JsCallback;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.pengirim.pengirim.HasilLacak;
import com.pengirim.pengirim.NothingSelectedSpinnerAdapter;
import com.pengirim.pengirim.R;
import com.pengirim.pengirim.services.Courier;
import com.pengirim.pengirim.utils.Const;
import com.pengirim.pengirim.utils.HistoryHandler;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LacakFragment extends Fragment implements TextWatcher{

    private InterstitialAd interstitial;
    AutoCompleteTextView etresi;
    //EditText etresi;
    Spinner spkurir;
    Button blacak;

    HistoryHandler historyHandler;

    String idresi;
    String tokens;
    String android_id;
    String codekurir;
    String recommenderjs;

    JsEvaluator jsEvaluator;
    ProgressDialog pDialog;

    SharedPreferences pref;
    public static final String MyPREFERENCES = "TokenPrefs";

    private static final String[] COUNTRIES = new String[] {
            //"Belgium", "France", "Italy", "Germany", "Spain"
    };

    public LacakFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_lacak, container, false);

        jsEvaluator = new JsEvaluator(getActivity());
        System.out.println("Starting service ...");
        historyHandler = new HistoryHandler(getActivity());

        getActivity().startService(new Intent(getActivity(),Courier.class));

        //baca file core.js
        recommenderjs = readCoreJs();

        /*Blok intersitial ads*/
        interstitial = new InterstitialAd(getActivity());
        interstitial.setAdUnitId("ca-app-pub-4178779359713838");
        android_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        AdView adView = (AdView) rootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(android_id)
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("CC5F2C72DF2B356BBF0DA198")
                .build();

        adView.loadAd(adRequest);
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                // Call displayInterstitial() function
                displayInterstitial();
            }
        });


        spkurir = ((Spinner) rootView.findViewById(R.id.spinnerkurir));
        //etresi = (EditText) rootView.findViewById(R.id.etresi);
        blacak = (Button) rootView.findViewById(R.id.blacak);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.kuriredit, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spkurir.setAdapter(
                new NothingSelectedSpinnerAdapter(
                        adapter,
                        R.layout.agen_spinner_row_nothing_selected,
                        // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                        getActivity()));

        //blok persiapan loading history
        List<String> allHistory = historyHandler.getAllHistory();
        for(String history : allHistory)
        {
            Log.d("Resi :", history);
        }

        etresi = (AutoCompleteTextView) rootView.findViewById(R.id.etresi);
        ArrayAdapter<String> adapterSpin = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, allHistory);
        etresi.setAdapter(adapterSpin);

        //set rekomender
        etresi.addTextChangedListener(this);

        //on enter pressed
        etresi.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            hideKeyboard();
                            idresi = etresi.getText().toString();
                            android_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);

                            pref = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                            tokens = pref.getString("token", "");

                            //Masukkan hasil pencarian ke dalam history

                            int total = historyHandler.getHistoryRow();
                            if(total < 6) {
                                historyHandler.addHistory(idresi);
                            }
                            else
                            {
                                historyHandler.deleteHistory();
                                historyHandler.addHistory(idresi);
                            }

                            //run pelacakan
                            new MyAsyncTask().execute(tokens, android_id, codekurir, idresi);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        etresi.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        spkurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String[] kurir = new String[]{
                        "jne","pos_indonesia","tiki",
                        "wahana","pandu_logistics","first_logistics",
                        "rpx_holding","ncs","dakota_cargo"
                };
//                codekurir = (String) parent.getItemAtPosition(pos);
                codekurir = kurir[pos];
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Button button = (Button) rootView.findViewById(R.id.blacak);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                idresi = etresi.getText().toString();
                android_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);

                pref = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                tokens = pref.getString("token", "");

                //Masukkan hasil pencarian ke dalam history

                int total = historyHandler.getHistoryRow();
                if(total < 6) {
                    historyHandler.addHistory(idresi);
                }
                else
                {
                    historyHandler.deleteHistory();
                    historyHandler.addHistory(idresi);
                }

                //run pelacakan
                new MyAsyncTask().execute(tokens, android_id, codekurir, idresi);
            }

        });
        return rootView;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (etresi.isPerformingCompletion()) {
            // An item has been selected from the list. Ignore.
            Toast.makeText(getActivity(), "Ada perubahan",
                    Toast.LENGTH_LONG).show();
            return;
        }
        // Your code for a general case
    }

    @Override
    public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
    }

    @Override
    public void afterTextChanged(final Editable s) {
        //Toast.makeText(getActivity(), etresi.getText().toString(),
               //Toast.LENGTH_LONG).show();

        //recommender here
        jsEvaluator.callFunction(recommenderjs,
            new JsCallback() {
            @Override
            public void onResult(final String result) {
                // Process result here.
                // This method is called in the UI thread.
                Toast.makeText(getActivity(), result ,Toast.LENGTH_SHORT).show();

                }
        }, "recomend", etresi.getText().toString());

    }

    public void displayInterstitial() {
        // If Ads are loaded, show Interstitial else show nothing.
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_example).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Memproses Pelacakan ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String response = postData(params[0], params[1], params[2], params[3]);
            System.out.print(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            //matikan dialog
            pDialog.dismiss();
            System.out.println("Post : " + result);
            try {
                JSONObject reader = new JSONObject(result);
                String data = reader.getString("response_code");
                System.out.println(data);
                if(data.equals("200"))
                {
                    //Toast.makeText(getActivity(), "Mencoba membuka halaman pelacakan",
                    //       Toast.LENGTH_LONG).show();
                    //save disini


                    SharedPreferences sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("uri", idresi);
                    editor.commit();


                    String isihtml= reader.getString("data");
                    if (saveToHTML(idresi, isihtml))
                    {
                        Toast.makeText(getActivity(), "Sukses menyimpan",
                                Toast.LENGTH_LONG).show();
                    }

                    //buka webview fragment
                    Intent intent = new Intent(getActivity(), HasilLacak.class);
                    startActivity(intent);

                }
                else
                {
                    String error = reader.getString("error");
                    if(error.equals("unauthorized")) {
                        alertBox("Sesi habis, silakan restart aplikasi untuk melakukan pelacakan");
                    }
                    else
                    {
                        alertBox(error);
                    }
                }
            }
            catch(JSONException jex)
            {
                jex.printStackTrace();
                alertBox("Komunikasi dengan server gagal");
            }

            etresi.setText("");
            spkurir.setSelection(0);

        }
    }

    public String postData(String token, String mobile_token, String courier_code, String waybill_code){

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Const.url_lacak);
        String text = "";

        try {
            // menambahkan param
            List nameValuePairs = new ArrayList(1);
            nameValuePairs.add(new BasicNameValuePair("token", token));
            nameValuePairs.add(new BasicNameValuePair("mobile_token", "mas_oky"));
            nameValuePairs.add(new BasicNameValuePair("courier_code", courier_code));
            nameValuePairs.add(new BasicNameValuePair("waybill_code", waybill_code));
            nameValuePairs.add(new BasicNameValuePair("format_type", "html"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // request
            HttpResponse response = httpclient.execute(httppost);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;

            while((current = bis.read()) != -1){
                baf.append((byte)current);
            }

            /* Convert the Bytes read to a String. */
            text = new String(baf.toByteArray());

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return text;
    }

    //tulis ke html
    public Boolean saveToHTML(String fname, String fcontent){
        try {

            //String fpath = "/storage/sdcard1/"+fname+".html";
            String fpath = "/storage/emulated/0/"+fname+".html";

            File file = new File(fpath);

            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fcontent);
            bw.close();

            Log.d("Sukses atuh", "Sukses atuh");
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public void alertBox(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setCancelable(false)
                .setTitle("Pengirim")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void hideKeyboard()
    {
        try
        {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        catch (Exception e)
        {
            // Ignore exceptions if any
            Log.e("KeyBoardUtil", e.toString(), e);
        }
    }

    public String readCoreJs(){

        BufferedReader br = null;
        String response = null;

        try {

            StringBuffer output = new StringBuffer();

            br = new BufferedReader(new InputStreamReader(getActivity().getAssets().open("core.js")));
            String line = "";
            while ((line = br.readLine()) != null) {
                output.append(line +"n");
            }
            response = output.toString();

        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }
        return response;
    }
}