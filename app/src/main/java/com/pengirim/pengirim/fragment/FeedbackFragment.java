package com.pengirim.pengirim.fragment;

/**
 * Created by Okyza Maherdy Prabowo on 31/01/2016.
 */

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.pengirim.pengirim.NothingSelectedSpinnerAdapter;
import com.pengirim.pengirim.R;
import com.pengirim.pengirim.utils.Const;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FeedbackFragment extends Fragment {

    public FeedbackFragment(){}

    String p1 = "<p> Punya pendapat, saran, kritik, curhatan, " +
            "usulan fitur, atau temuan bug di Pengirim? Sampaikan melalui form ini:</p>";
    String p2 = "Punya pendapat, saran, kritik, curhatan, " +
            "usulan fitur, atau temuan bug di Pengirim? Sampaikan melalui form ini:";

    String nama = "";
    String surel = "";
    String konten = "";
    String tipe = "";
    String tokens = "";
    String mobiltok = "";
    Spinner spinner;
    EditText kontaknama;
    EditText isifeedback;
    ProgressDialog pDialog;

    SharedPreferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_feedback, container, false);

        TextView view = (TextView) rootView.findViewById(R.id.txtpar1);
        Spanned result = Html.fromHtml(p1);
        view.setText(p2);

        spinner = ((Spinner) rootView.findViewById(R.id.spinnerjenis));
        kontaknama = (EditText) rootView.findViewById(R.id.etkontak);
        isifeedback = (EditText) rootView.findViewById(R.id.etisi);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.feedbacklist, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(
                new NothingSelectedSpinnerAdapter(
                        adapter,
                        R.layout.feedback_spinner_row_nothing_selected,
                        // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                        getActivity()));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                tipe = (String) parent.getItemAtPosition(pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Button button = (Button) rootView.findViewById(R.id.bkirim);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                nama = kontaknama.getText().toString();
                surel = kontaknama.getText().toString();
                konten = isifeedback.getText().toString();
                mobiltok = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);

                pref = getActivity().getSharedPreferences(Const.MyPREFERENCES, Context.MODE_PRIVATE);
                tokens = pref.getString("token", "");

                System.out.println(surel);
                System.out.println(nama);
                System.out.println(konten);
                System.out.println(mobiltok);
                System.out.println(tokens);
                System.out.println(tipe);
                new MyAsyncTask().execute(nama, surel, konten, tipe, tokens, mobiltok);
            }

        });

        return rootView;
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Memproses Feedback ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String response = postData(params[0], params[1], params[2], params[3], params[4], params[5]);
            System.out.print(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            // tutup dialog
            pDialog.dismiss();
            System.out.println("Post : " + result);
            try {
                JSONObject reader = new JSONObject(result);
                String data = reader.getString("response_code");
                System.out.println(data);

                if(data.equals("200"))
                {
                    alertBox("Feedback berhasil dikirim");
                }
                else
                {
                    String error = reader.getString("error");
                    alertBox("Feedback tidak dapat diproses");
                }
            }
            catch(JSONException jex)
            {
                jex.printStackTrace();
                //Toast.makeText(getActivity(),"Komunikasi dengan server gagal", Toast.LENGTH_LONG);
                alertBox("Komunikasi dengan server gagal");
            }

            kontaknama.setText("");
            isifeedback.setText("");
            spinner.setSelection(0);

        }

    }

    public String postData(String name, String email, String content, String type, String token, String mobile_token ){

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Const.url_feedback);
        String text = "";

        try {
            // menambahkan param
            List nameValuePairs = new ArrayList(1);
            nameValuePairs.add(new BasicNameValuePair("name", name));
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("content", content));
            nameValuePairs.add(new BasicNameValuePair("type_feedback", type));
            nameValuePairs.add(new BasicNameValuePair("token", token));
            nameValuePairs.add(new BasicNameValuePair("mobile_token", mobile_token));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // request
            HttpResponse response = httpclient.execute(httppost);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;

            while((current = bis.read()) != -1){
                baf.append((byte)current);
            }

            /* Convert the Bytes read to a String. */
            text = new String(baf.toByteArray());


        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return text;
    }

    public void alertBox(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setCancelable(false)
                .setTitle("Pengirim")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
