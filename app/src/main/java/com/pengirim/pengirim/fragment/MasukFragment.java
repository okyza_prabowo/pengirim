package com.pengirim.pengirim.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.pengirim.pengirim.R;
import com.pengirim.pengirim.utils.Const;

/**
 * Created by Okyza Maherdy Prabowo on 22/03/2016.
 */
public class MasukFragment extends Fragment {

    private WebView mWebView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.fragment_daftar, container, false);
        mWebView = (WebView) v.findViewById(R.id.web_view);

        final ProgressDialog pd = ProgressDialog.show(getActivity(), "", "Memuat halaman daftar ...", true);
        mWebView.loadUrl(Const.url_masuk);

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mWebView.setWebViewClient(new WebViewClient(){
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                pd.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();

            }
        });

        return v;
    }
}
