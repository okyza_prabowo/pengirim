/*----------------------------Sistem REKOMENDER Pengirim.com  
	@Versi 0.0.2
	@Teknimo Cipta Inovasi
	@By : Adnan HP
	@Dibuat 14 Oktober 2014, Revisi Terakhir 29 April 2016
	@2014-2016 Hak Cipta Terlindungi 
	
	Sistem Rekomender Pengirim.com adalah modul yang berfungsi untuk merekomendasikann
	urutan agen pengiriman dan memilih yang memiliki probabilitas paling besar
	
	Update : 
	- Penambahan angka untuk resi
	- 29 April : Penambahan rekomendasi sicepat dan REX

*/
/*Fungsi agen  */
function Agent(name, weight, number) {
    this.name = name;
    this.number = number;
    this.weight = weight;
    this.weighted=weight;
    this.add = function(value) {
    	this.weighted=this.weighted+value;
    };
    this.min = function(value){
    	this.weighted = this.weighted-value;
    };
    this.reset=function(){
    	this.weighted=this.weight;
    }
}


var jne=new Agent('JNE', 5, 1),
	pos=new Agent('Pos Indonesia', 4, 2),
	whn=new Agent('Wahana', 3, 3),
	tiki=new Agent('TIKI', 4, 4),
	pnd=new Agent('Pandu Logistics', 3, 5),
	frs=new Agent('First Logistics', 3, 6),
	rpx=new Agent('RPX Holding', 2, 7),
	ncs=new Agent('NCS', 3, 8),
	dkt=new Agent('Dakota Cargo', 3, 9),
	ems=new Agent('EMS Pos Indonesia', 4, 10),
	esl=new Agent('ESL Express', 4, 11),
	chy=new Agent('Cahaya Logistik', 1, 12),
	rex = new Agent('REX Kiriman Express', 3, 13),
	scpt = new Agent('Sicepat', 3, 14);


/*Fungsi Utama 
	Mengembalikan obyek yang berisi 2 data
	1. Array yang diurutkan dengan nama array
	2. Agen yang dipilih, dengan nama Selected;
*/
function recomend(input){
	// console.clear();
	// console.log(input);

	resetValue();
	if(input==undefined) return;

	var length=input.length;
	var agentCopy;
	var letterRegex='^[a-zA-Z]+$';
	if(input.length=0 || input==undefined){
		return buildSort();
	}
	//Prioritaskan untuk EMS dan JNE
	//Jika  2 karakter pertama huruf

	if(input.substr(0,2).match(letterRegex)){
	  //jika karakter ketiga angka
	  	if(!input.substr(2, 1).match(letterRegex)){
			ems.add(5);	  	 
	  	}else{
	  		jne.add(5);
	  	}
	  	return buildSort();

	}
	//untuk Wahana
	//Ciri khas wahana menggunakan angka 88 didepan
	// console.log('substring wahana '+input.substr(0,2));

	if(input.substr(0,2)=='88'){
		whn.add(5);
	}


	//Untuk REX 
	//Ciri khas rex adalah menggunakan angka 777 didepan
	if(input.substr(0,3)=='777'){
		rex.add(5);
	}

	//untuk dakota dan sicepat 
	//Untuk sicepat, jumlah 0 ada 4, dan dakota jumlah 0 ada 5
	// console.log('substring dakota '+input.substr(0,5));
	
	if(input.substr(0,4)=='0000'){
		dkt.add(5);
		scpt.add(5);
	}	

	if(input.substr(0,5)=='00000'){
		dkt.add(5);
		scpt.min(5);

	}



	if(length==13){
		esl.add(5);
		whn.add(5);
		chy.add(5);
		jne.add(5);
	}
	//Jika panjangnya 12
	if(length==12){
		rpx.add(5);
		rex.add(5);
		scpt.add(5);
	}
	//Jika panjangnya 11
	if(length==11){
		tiki.add(5);
		ncs.add(5);
		pos.add(5);
		pnd.add(5);
	}
	if(length==9){
		frs.add(5);
	}
	return buildSort();
}

/* ----  Fungsi untuk mengurutkan dan mengembalikan resi terpilih dan urutan resinya*/
function buildSort(){
	var array=[jne, tiki, rpx, ncs, pos, chy, whn, /* esl */, dkt, ems, frs, pnd, scpt, rex];
	var array_sort=sort(array);

	// console.log('selected '+array_sort[0].name)
	// for (var i = 0; i < array_sort.length; i++) {
	// 	console.log('Sort '+array_sort[i].name+' '+array_sort[i].weighted);
	// }
	return {selected:array_sort[0].name, array:array_sort};

}
//Mengurutkan berdasarkan bobot
function sort(array) {
    return array.sort(function(a, b) {
        var x = a.weighted; 
        var y = b.weighted;
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}


function resetValue(){
	jne.reset();
	tiki.reset();
	ncs.reset();
	pos.reset();
	chy.reset();
	whn.reset();
	esl.reset();
	dkt.reset();
	ems.reset();
	frs.reset();
	pnd.reset();
	rpx.reset();
	rex.reset();
	scpt.reset();
}
